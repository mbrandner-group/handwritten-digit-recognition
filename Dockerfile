FROM tensorflow/tensorflow:latest-gpu
COPY requirements.txt requirements.txt
RUN pip install --root-user-action=ignore -r requirements.txt
# yes, we use that dirty way now, interesting when this did work without in earlier images
# https://stackoverflow.com/questions/68673221/warning-running-pip-as-the-root-user
